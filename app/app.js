'use strict';

const NavigationExampleRow = require('./NavigationExampleRow');
const React = require('react');
const ReactNative = require('react-native');
import state from './state'
import actions from './actions'
import BackButton from './BackButton'
import { observer }from 'mobx-react/native';
import Home from './Home'

import {
  Button
} from 'react-native-elements'

// import {
//   Components,
// } from 'exponent';

const {
  NavigationExperimental,
  ScrollView,
  StyleSheet,
  View,
  StatusBar,
  TouchableOpacity,
  Text,
  Platform,
  Image,
  Modal
} = ReactNative;

const {
  CardStack: NavigationCardStack,
  StateUtils: NavigationStateUtils,
  Header: NavigationHeader,
} = NavigationExperimental;

StatusBar.setBarStyle("light-content")
state.loadState()


@observer class App extends React.Component {
  render(): ReactElement {
    if (!state.isLoaded) return <View/>


    return (
      <Navigator
        navigationState={state.navigationState}
        onNavigationChange={actions.navigate}
      />
    );
  }

  handleBackAction(): boolean {
    return actions.pop()
  }
}

@observer class Navigator extends React.Component {
  render(): ReactElement {
    return (
        <NavigationCardStack
          onNavigateBack={actions.pop}
          navigationState={JSON.parse(JSON.stringify(state.navigationState))}
          renderHeader={this.renderHeader}
          renderScene={this.renderScene}
          style={styles.navigator} />
    );
  }

  renderScene = (sceneProps: Object): ReactElement =>  {
    return (
      <Home
        route={sceneProps.scene.route}
        onPushRoute={actions.push}
        onPopRoute={actions.pop}
      />
    );
  }

  renderHeader = (sceneProps: Object): ReactElement => {
    return (
      <Header
        {...sceneProps}
      />
    );
  }

}


class Header extends React.Component {

  render(): ReactElement {
    return (
      <NavigationHeader
        {...this.props}
        style = {styles.navBar}
        renderTitleComponent={this.renderTitleComponent}
        renderLeftComponent = {this.renderLeftComponent}
        renderRightComponent = {this.renderRightComponent}
        onNavigateBack={this.back}
      />
    );
  }

  back = (): void => {
    actions.pop();
  }

  renderLeftComponent(props: Object): ReactElement {
    if (props.scene.index != 0) return <BackButton onPress = {actions.pop} imageStyle = {styles.backButton} />
  }

  renderRightComponent(props) {
    if (props.scene.index === 0) return (
      <TouchableOpacity style = {styles.addButtonContainer} onPress = {actions.presentTodoModal}>
        <Image style={styles.addButton} source={require('./assets/plus.png')} />
      </TouchableOpacity>
    )
  }

  renderTitleComponent = (props: Object): ReactElement => {
    return (
      <NavigationHeader.Title textStyle = {styles.navigationTitle}>
        {props.scene.route.key}
      </NavigationHeader.Title>
    );
  }
}

const styles = StyleSheet.create({
  navigator: {
    flex: 1,
  },
  navBar: {
    backgroundColor: '#262523',
  },
  navigationTitle: {
    color: '#FFFFFF'
  },
  backButton: {
    tintColor: "#FFFFFF"
  },
  addButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButton: {
    tintColor: '#FFFFFF',
    height: 24,
    width: 24,
    margin: Platform.OS === 'ios' ? 10 : 16,
    resizeMode: 'contain',
  },
  modal: {
    margin: 30,
    marginTop: 100,
    marginBottom: 100,
    flex: 1, 
    backgroundColor: 'white',
  },
  modalBackground: {
    flex: 1, 
    backgroundColor: 'rgba(52,52,52,.2)'
  }
});

module.exports = App;