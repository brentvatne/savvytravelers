import {observable, observe} from 'mobx';
const ReactNative = require('react-native');

import  {
  AsyncStorage
} from 'react-native';

var state = observable({
    navigationState: {
        index: 0, 
        routes: [{key: 'Home'}], 
        },
    newListModalVisibile: false,
    isLoaded: false,
    todoLists: []
})

state.saveState = observe(state.todoLists, () => {
    console.log("save")
    var stateToSave = { todoLists: state.todoLists }
    AsyncStorage.setItem("state", JSON.stringify(stateToSave));
})

state.loadState = () => {
    AsyncStorage.getItem("state").then((value) => {
        value = JSON.parse(value)
        if (value) state = {...state, ...value}
        state.isLoaded = true
    }).done();
}


module.exports = state;