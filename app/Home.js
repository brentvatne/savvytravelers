'use strict';

const NavigationExampleRow = require('./NavigationExampleRow');
const React = require('react');
const ReactNative = require('react-native');
import state from './state'
import actions from './actions'
import { observer }from 'mobx-react/native';

import {
  Button
} from 'react-native-elements'

// import {
//   Components,
// } from 'exponent';

const {
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Modal
} = ReactNative;

@observer class Home extends React.Component {
  render() {
    return (
      <View>
        <Modal
            animationType={"fade"}
            transparent={true}
            visible={state.newListModalVisibile}
            >
          <View style = {styles.modalBackground}>
          <View style = {styles.modal}>
            <Button
              raised
              title='Create' 
              onPress = {actions.dismissTodoModal}/>
          </View>
          </View>
          </Modal>
        <ScrollView>
          <NavigationExampleRow
            text={'route = ' + this.props.route.key}
          />
          <NavigationExampleRow
            text="Push Route"
            onPress={actions.push}
          />
          <NavigationExampleRow
            text="Pop Route"
            onPress={actions.pop}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    margin: 30,
    marginTop: 100,
    marginBottom: 100,
    flex: 1, 
    backgroundColor: 'white',
  },
  modalBackground: {
    flex: 1, 
    backgroundColor: 'rgba(52,52,52,.2)'
  }
});

module.exports = Home;