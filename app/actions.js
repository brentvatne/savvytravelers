import state from './state'
import { observable, computed, action } from 'mobx';
const ReactNative = require('react-native');

const {
  NavigationExperimental,
} = ReactNative;

const {
  CardStack: NavigationCardStack,
  StateUtils: NavigationStateUtils,
} = NavigationExperimental;


class Actions {

  @action navigate(type, key) {
    let {navigationState} = state;
    switch (type) {
      case 'push':
        const route = {key: 'route-' + Date.now()};
        navigationState = NavigationStateUtils.push(navigationState, route);
        break;

      case 'pop':
        navigationState = NavigationStateUtils.pop(navigationState);
        break;
    }

    if (state.navigationState !== navigationState) {
      state.navigationState = navigationState;
    }
  }

  push = action((key) => {
    this.navigate('push', key);
  })

  pop = action(() => {
    this.navigate('pop');
  })

  @action presentTodoModal() {
    state.newListModalVisibile = true
  }

  @action dismissTodoModal() {
    state.newListModalVisibile = false
  }

}

module.exports = new Actions()